var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var del = require('del');
var es = require('event-stream');
var bowerFiles = require('main-bower-files');
var print = require('gulp-print');
var Q = require('q');
// var connect = require('gulp-connect');
var jenkins = require('gulp-jenkins');
// var bs = require("browser-sync").create();
// var serve = require('gulp-serve');
var browserSync = require('browser-sync');
var livereload = require('gulp-livereload');
var webserver = require('gulp-webserver');
var pump = require('pump');
var gp_concat = require('gulp-concat'),
    gp_rename = require('gulp-rename'),
    gp_uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var concatCss = require('gulp-concat-css');
var sass = require('gulp-sass');
var less = require('gulp-less');
var minify = require('gulp-minify-css');
var merge = require('merge-stream');
// var nodemon = require('gulp-nodemon');
// var htmlSnapshots = require('html-snapshots');
var htmlmin = require('gulp-htmlmin');


var reload = browserSync.reload;
// == PATH STRINGS ========

var paths = {
    scripts: 'prodServer/**/*.js',
    styles: ['./prodServer/**/*.css', './prodServer/**/*.scss'],
    images: './images/**/*',
    index: './prodServer/index.html',
    partials: ['prodServer/**/*.html', '!prodServer/index.html'],
    distDev: './dist.dev',
    distProd: './dist.prod',
    distScriptsProd: './dist.prod/scripts',
    scriptsDevServer: 'devServer/**/*.js',

};

// == PIPE SEGMENTS ========

var pipes = {};

pipes.orderedVendorScripts = function() {
    return plugins.order(['jquery.js', 'angular.js']);
};

pipes.orderedAppScripts = function() {
    return plugins.angularFilesort();
};

pipes.minifiedFileName = function() {
    return plugins.rename(function (path) {
        path.extname = '.min' + path.extname;
    });
};

pipes.validatedAppScripts = function() {
    return gulp.src(paths.scripts)
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('jshint-stylish'));
};

pipes.builtAppScriptsDev = function() {
    return pipes.validatedAppScripts()
        .pipe(gulp.dest(paths.distDev));
};

pipes.builtAppScriptsProd = function() {
    var scriptedPartials = pipes.scriptedPartials();
    var validatedAppScripts = pipes.validatedAppScripts();

    return es.merge(scriptedPartials, validatedAppScripts)
        .pipe(pipes.orderedAppScripts())
        .pipe(plugins.sourcemaps.init())
            .pipe(plugins.concat('app.min.js'))
            .pipe(plugins.uglify())
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest(paths.distScriptsProd));
};

pipes.builtVendorScriptsDev = function() {
    return gulp.src(bowerFiles())
        .pipe(gulp.dest('dist.dev/bower_components'));
};

pipes.builtVendorScriptsProd = function() {
    return gulp.src(bowerFiles('**/*.js'))
        .pipe(pipes.orderedVendorScripts())
        .pipe(plugins.concat('vendor.min.js'))
        .pipe(plugins.uglify())
        .pipe(gulp.dest(paths.distScriptsProd));
};

pipes.validatedDevServerScripts = function() {
    return gulp.src(paths.scriptsDevServer)
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('jshint-stylish'));
};

pipes.validatedPartials = function() {
    return gulp.src(paths.partials)
        .pipe(plugins.htmlhint({'doctype-first': false}))
        .pipe(plugins.htmlhint.reporter());
};

pipes.builtPartialsDev = function() {
    return pipes.validatedPartials()
        .pipe(gulp.dest(paths.distDev));
};

pipes.scriptedPartials = function() {
    return pipes.validatedPartials()
        .pipe(plugins.htmlhint.failReporter())
        .pipe(plugins.htmlmin({collapseWhitespace: true, removeComments: true}))
        .pipe(plugins.ngHtml2js({
            moduleName: "healthyGulpAngularApp"
        }));
};

pipes.builtStylesDev = function() {
    return gulp.src(paths.styles)
        .pipe(plugins.sass())
        .pipe(gulp.dest(paths.distDev));
};

pipes.builtStylesProd = function() {
    return gulp.src(paths.styles)
        .pipe(plugins.sourcemaps.init())
            .pipe(plugins.sass())
            .pipe(plugins.minifyCss())
        .pipe(plugins.sourcemaps.write())
        .pipe(pipes.minifiedFileName())
        .pipe(gulp.dest(paths.distProd));
};

pipes.processedImagesDev = function() {
    return gulp.src(paths.images)
        .pipe(gulp.dest(paths.distDev + '/images/'));
};

pipes.processedImagesProd = function() {
    return gulp.src(paths.images)
        .pipe(gulp.dest(paths.distProd + '/images/'));
};

pipes.validatedIndex = function() {
    return gulp.src(paths.index)
        .pipe(plugins.htmlhint())
        .pipe(plugins.htmlhint.reporter());
};

pipes.builtIndexDev = function() {

    var orderedVendorScripts = pipes.builtVendorScriptsDev()
        .pipe(pipes.orderedVendorScripts());

    var orderedAppScripts = pipes.builtAppScriptsDev()
        .pipe(pipes.orderedAppScripts());

    var appStyles = pipes.builtStylesDev();

    return pipes.validatedIndex()
        .pipe(gulp.dest(paths.distDev)) // write first to get relative path for inject
        .pipe(plugins.inject(orderedVendorScripts, {relative: true, name: 'bower'}))
        .pipe(plugins.inject(orderedAppScripts, {relative: true}))
        .pipe(plugins.inject(appStyles, {relative: true}))
        .pipe(gulp.dest(paths.distDev));
};

pipes.builtIndexProd = function() {

    var vendorScripts = pipes.builtVendorScriptsProd();
    var appScripts = pipes.builtAppScriptsProd();
    var appStyles = pipes.builtStylesProd();

    return pipes.validatedIndex()
        .pipe(gulp.dest(paths.distProd)) // write first to get relative path for inject
        .pipe(plugins.inject(vendorScripts, {relative: true, name: 'bower'}))
        .pipe(plugins.inject(appScripts, {relative: true}))
        .pipe(plugins.inject(appStyles, {relative: true}))
        .pipe(plugins.htmlmin({collapseWhitespace: true, removeComments: true}))
        .pipe(gulp.dest(paths.distProd));
};

pipes.builtAppDev = function() {
    return es.merge(pipes.builtIndexDev(), pipes.builtPartialsDev(), pipes.processedImagesDev());
};

pipes.builtAppProd = function() {
    return es.merge(pipes.builtIndexProd(), pipes.processedImagesProd());
};

// == TASKS ========

// removes all compiled dev files
gulp.task('clean-dev', function() {
    var deferred = Q.defer();
    del(paths.distDev, function() {
        deferred.resolve();
    });
    return deferred.promise;
});

// removes all compiled production files
gulp.task('clean-prod', function() {
    var deferred = Q.defer();
    del(paths.distProd, function() {
        deferred.resolve();
    });
    return deferred.promise;
});


// gulp.task('connect', function() {
//   connect.server({
//     root: 'app',
//     livereload: true
//   });
// });

// gulp.task('jenkins-tests', function() {
//   connect.server({
//     port: 3000
//   });
//   // run some headless tests with phantomjs 
//   // when process exits: 
//   connect.serverClose();
// });

// gulp.task('serve', [], function() {
//   // .init starts the server
//   bs.init({
//     server: "./app",
//     port: 3000
//   });
// });
 
// gulp.task('serve', serve('public'));
// gulp.task('serve-build', serve(['public', 'build']));
// gulp.task('serve-prod', serve({
//   root: ['public', 'build'],
//   port: 3000,
//   middleware: function(req, res) {
//     // custom optional middleware 
//   }
// }));

// gulp.task('serve', function() {
//   livereload.listen(1234);

//   browserSync({
//     server: {
//       baseDir: 'app',
//     },
//    port: 3000
//   });
//   gulp.watch(['*.html', 'styles/**/*.css', 'scripts/**/*.js'], {cwd: 'app'}, reload);
// });

gulp.task('uglify', function(){
    return gulp.src([
                      'app/bower_components/angular/angular.min.js', 
                      'app/bower_components/simple-web-notification/web-notification.js', 
                      'app/bower_components/angular-web-notification/angular-web-notification.js', 
                      'app/bower_components/angular-resource/angular-resource.min.js', 
                      'app/bower_components/angular-ui-router/release/angular-ui-router.min.js', 
                      'app/bower_components/angular-sanitize/angular-sanitize.min.js', 
                      'app/bower_components/angular-animate/angular-animate.min.js', 
                      'app/bower_components/angular-touch/angular-touch.js', 
                      'app/bower_components/angular-cookies/angular-cookies.min.js', 
                      'app/bower_components/angularjs-slider/dist/rzslider.min.js', 
                      'app/bower_components/angular-bootstrap/ui-bootstrap.min.js', 
                      'app/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js', 
                      'app/bower_components/angular-google-places-autocomplete/dist/autocomplete.min.js', 
                      'app/bower_components/ng-file-upload/ng-file-upload-shim.min.js', 
                      'app/bower_components/ng-file-upload/ng-file-upload.min.js', 
                      'app/bower_components/angular-marquee/src/marquee.js',
                      'app/scripts/app.js',
                      'app/scripts/services.js',
                      'app/scripts/home.js',
                      'app/scripts/controllers/*.js',
                      'app/scripts/controllers/*/*.js'
        ]) 
    .pipe(gp_concat('scripts/all.js'))
    .pipe(gulp.dest('prodServer'))
    .pipe(gp_rename('scripts/all.min.u.js'))
    .pipe(gp_uglify())
    .pipe(gulp.dest('prodServer'));
});


// gulp.task('css', function () {
//   gulp.src([
//     'app/bower_components/bootstrap/dist/css/bootstrap.min.css',
//     'app/bower_components/Font-Awesome/css/font-awesome.css',
//     'app/bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css',
//     'app/bower_components/angularjs-slider/dist/rzslider.min.css',
//     'app/styles/libs/style.css',
//     'app/styles/scroll.css'

//     ])
//     .pipe(uglifycss({
//       "maxLineLen": 80,
//       "uglyComments": true
//     }))
//     .pipe(gp_concat('styles/all.min.u.css'))
//     .pipe(minify())
//     .pipe(gulp.dest('./app'));
// });


gulp.task('default', ['js-fef'], function(){});



gulp.task('build', function() {

    var lessStream = gulp.src([])
        .pipe(less())
        .pipe(gp_concat('less-files.less'))
    ;

    var scssStream = gulp.src([])
        .pipe(sass())
        .pipe(gp_concat('scss-files.scss'))
    ;
    
    var cssStream = gulp.src([
        'app/bower_components/bootstrap/dist/css/bootstrap.min.css',
        'app/bower_components/Font-Awesome/css/font-awesome.css',
        'app/bower_components/angular-google-places-autocomplete/dist/autocomplete.min.css',
        'app/bower_components/angularjs-slider/dist/rzslider.min.css',
        'app/styles/libs/cozy-real-estate-font.css',
        'app/styles/libs/owl.transitions.css.css',
        'app/styles/libs/style.css',
        'app/styles/libs/font-awesome.css',
        'app/styles/scroll.css'

        ])
        .pipe(gp_concat('all.minc.css'))
    ;

    var mergedStream = merge(lessStream, scssStream, cssStream)
        .pipe(gp_concat('styles.css'))
        .pipe(minify())
        .pipe(gulp.dest('prodServer/styles'));

    return mergedStream;
});


gulp.task('htminv', function() {
  return gulp.src('./app/views/**/*.html' )
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./prodServer/views'));
});

gulp.task('htmino', function() {
  return gulp.src('./app/*.html' )
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./prodServer'));
});


gulp.task('serve', function() {
  gulp.src('app')
  .pipe(webserver({
    fallback:   'index.html',
    livereload: true,
    directoryListing: false,
    open: true,
    port: 4000
  }));
    gulp.watch(['*.html', 'styles/**/*.css', 'scripts/**/*.js'], {cwd: 'app'}, reload);

    
});



gulp.task('default', ['webserver']);

gulp.task('default', ['connect']);
// checks html source files for syntax errors
gulp.task('validate-partials', pipes.validatedPartials);

// checks index.html for syntax errors
gulp.task('validate-index', pipes.validatedIndex);

// moves html source files into the dev environment
gulp.task('build-partials-dev', pipes.builtPartialsDev);

// converts partials to javascript using html2js
gulp.task('convert-partials-to-js', pipes.scriptedPartials);

// runs jshint on the dev server scripts
gulp.task('validate-devserver-scripts', pipes.validatedDevServerScripts);

// runs jshint on the app scripts
gulp.task('validate-app-scripts', pipes.validatedAppScripts);

// moves app scripts into the dev environment
gulp.task('build-app-scripts-dev', pipes.builtAppScriptsDev);

// concatenates, uglifies, and moves app scripts and partials into the prod environment
gulp.task('build-app-scripts-prod', pipes.builtAppScriptsProd);

// compiles app sass and moves to the dev environment
gulp.task('build-styles-dev', pipes.builtStylesDev);

// compiles and minifies app sass to css and moves to the prod environment
gulp.task('build-styles-prod', pipes.builtStylesProd);

// moves vendor scripts into the dev environment
gulp.task('build-vendor-scripts-dev', pipes.builtVendorScriptsDev);

// concatenates, uglifies, and moves vendor scripts into the prod environment
gulp.task('build-vendor-scripts-prod', pipes.builtVendorScriptsProd);

// validates and injects sources into index.html and moves it to the dev environment
gulp.task('build-index-dev', pipes.builtIndexDev);

// validates and injects sources into index.html, minifies and moves it to the dev environment
gulp.task('build-index-prod', pipes.builtIndexProd);

// builds a complete dev environment
gulp.task('build-app-dev', pipes.builtAppDev);

// builds a complete prod environment
gulp.task('build-app-prod', pipes.builtAppProd);

// cleans and builds a complete dev environment
gulp.task('clean-build-app-dev', ['clean-dev'], pipes.builtAppDev);

// cleans and builds a complete prod environment
gulp.task('clean-build-app-prod', ['clean-prod'], pipes.builtAppProd);

// clean, build, and watch live changes to the dev environment
gulp.task('watch-dev', ['clean-build-app-dev', 'validate-devserver-scripts'], function() {

    // start nodemon to auto-reload the dev server
    plugins.nodemon({ script: 'server.js', ext: 'js', watch: ['devServer/'], env: {NODE_ENV : 'development'} })
        .on('change', ['validate-devserver-scripts'])
        .on('restart', function () {
            console.log('[nodemon] restarted dev server');
        });

    // start live-reload server
    plugins.livereload.listen({ start: true });

    // watch index
    gulp.watch(paths.index, function() {
        return pipes.builtIndexDev()
            .pipe(plugins.livereload());
    });

    // watch app scripts
    gulp.watch(paths.scripts, function() {
        return pipes.builtAppScriptsDev()
            .pipe(plugins.livereload());
    });

    // watch html partials
    gulp.watch(paths.partials, function() {
        return pipes.builtPartialsDev()
            .pipe(plugins.livereload());
    });

    // watch styles
    gulp.watch(paths.styles, function() {
        return pipes.builtStylesDev()
            .pipe(plugins.livereload());
    });

});

// clean, build, and watch live changes to the prod environment
gulp.task('watch-prod', ['clean-build-app-prod', 'validate-devserver-scripts'], function() {

    // start nodemon to auto-reload the dev server
    plugins.nodemon({ script: 'server.js', ext: 'js', watch: ['devServer/'], env: {NODE_ENV : 'production'} })
        .on('change', ['validate-devserver-scripts'])
        .on('restart', function () {
            console.log('[nodemon] restarted dev server');
        });

    // start live-reload server
    plugins.livereload.listen({start: true});

    // watch index
    gulp.watch(paths.index, function() {
        return pipes.builtIndexProd()
            .pipe(plugins.livereload());
    });

    // watch app scripts
    gulp.watch(paths.scripts, function() {
        return pipes.builtAppScriptsProd()
            .pipe(plugins.livereload());
    });

    // watch hhtml partials
    gulp.watch(paths.partials, function() {
        return pipes.builtAppScriptsProd()
            .pipe(plugins.livereload());
    });

    // watch styles
    gulp.watch(paths.styles, function() {
        return pipes.builtStylesProd()
            .pipe(plugins.livereload());
    });

});

// default task builds for prod
gulp.task('default', ['clean-build-app-prod']);
